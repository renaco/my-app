import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
  // template: '<label>Hola {{ this.name }} {{ this.dishes }}</label>',
export class HelloWorldComponent implements OnInit {
    name: string;
    dishes: string[];

  constructor() { 
    this.name = 'renaco';
    this.dishes = ['cuy', 'ceviche', 'carapulcra']
  }

  ngOnInit() {
  }

}
